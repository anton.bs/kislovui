import {ReactNode} from "react";

export type SelectItem = {
    label: ReactNode,
    value: string
}