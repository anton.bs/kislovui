export const concatClasses = (classes: string[]) => {
    const removeEmpty = (classesArray: string[]): string[] => {
        return classesArray.filter(str => str.trim() !== '');
    }
    if (removeEmpty(classes).length > 1) {
        return classes.join(" ")
    } else {
        return classes.join("");
    }
}