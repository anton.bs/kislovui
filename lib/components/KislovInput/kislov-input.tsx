import {ChangeEvent, ReactNode, useState} from 'react';
import './kislov-input.scss';
import {concatClasses} from "../../helpers";

interface KislovInputProps {
    label?: string,
    value?: string,
    type?: "text" | "multiline" | "password",
    onChange?: (value: string) => void,
    size?: "small" | "medium" | "large",
    rows?: number,
    icon?: ReactNode,
    action?: () => void,
    disabled?: boolean,
    customClasses?: string,
}
export const KislovInput = ({
    label = '',
    value = '',
    type = "text",
    onChange,
    size = "large",
    rows = 3,
    icon,
    action,
    disabled = false,
    customClasses
                            }: KislovInputProps) => {

    const [inputValue, setInputValue] = useState<string>(value || '');

    const changeInputValue = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setInputValue(event.target.value)
        onChange?.(event.target.value);
    }

    const getInputElement = () => {
        switch (type) {
            case "text":
            case "password":
                return <input disabled={disabled} type={type} value={inputValue} onChange={(e) => changeInputValue(e)} />
            case "multiline":
                return <textarea rows={rows} disabled={disabled} value={inputValue} onChange={(e) => changeInputValue(e)} />
        }
    }

    return (
        <div className={concatClasses(["kislov-input--wrapper", customClasses ? customClasses : ''])}>
            <div className={`kislov-input ${size}${disabled ? ' disabled' : ''}`}>
                {getInputElement()}
                <div className={`kislov-input--label${inputValue.length > 0 ? ' touched' : ''}`}>{label}</div>
                {icon && <div className={`kislov-input--icon${action ? ' clickable' : ''}`}>{icon}</div> }
            </div>
        </div>
    );
};
