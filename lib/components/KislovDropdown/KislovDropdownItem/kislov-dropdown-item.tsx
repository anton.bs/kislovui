import {ReactNode} from 'react';
import './kislov-dropdown-item.scss';

interface KislovDropdownItemProps {
    children: ReactNode,
    action?: () => void,
    isSelected?: boolean,
}
export const KislovDropdownItem = ({
                                children, action, isSelected = false,
                            }: KislovDropdownItemProps) => {

    return (
        <div className="kislov-dropdown-item--wrapper">
            <div className={`kislov-dropdown-item${isSelected ? ' is-selected' : ''}`} onClick={() => action?.()}>
                {children}
            </div>
        </div>
    );
};
