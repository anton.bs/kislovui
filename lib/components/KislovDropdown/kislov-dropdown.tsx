import {ReactNode, RefObject, useEffect, useRef, useState} from 'react';
import './kislov-dropdown.scss';
import {KislovDropdownItem} from "./KislovDropdownItem/kislov-dropdown-item";
import {concatClasses} from "../../helpers";
import {SelectItem} from "../../types";

interface KislovDropdownProps {
    items: { label: ReactNode, value: string }[],
    selectionChange?: (selectedItem: SelectItem) => void,
    selectedValue?: string | null,
    customClasses?: string,
    size: "small" | "medium" | "large"
}

export const KislovDropdown = ({
                                items = [], selectionChange, selectedValue, customClasses, size = "large"
                            }: KislovDropdownProps) => {

    const [dropdownPosition, setDropdownPosition] = useState({top: "100%"})

    const dropdownRef: RefObject<HTMLDivElement> = useRef(null);
    const selectItem = (item: SelectItem) => {
        selectionChange?.(item);
    }

    const calculateDropdownPosition = () => {
        if (dropdownRef.current) {
            const dropdownRect = dropdownRef.current.getBoundingClientRect();
            const spaceBelow = window.innerHeight - dropdownRect.bottom;

            if (spaceBelow < dropdownRect.height) {
                setDropdownPosition({top: `-${dropdownRect.height}px`});
            }
        }
    }

    useEffect(() => {
        calculateDropdownPosition()
    }, [])

    return (
        <div className={concatClasses(["kislov-dropdown--wrapper", customClasses ? customClasses : ''])} ref={dropdownRef} style={dropdownPosition}>
            <div className={`kislov-dropdown ${size}`}>
                {items.length === 0 && <span className="no-items">No items found</span> }
                {items.map((item, index) =>
                    <KislovDropdownItem action={() => selectItem(item)} key={`dropdown_item_${index}`} isSelected={item.value === selectedValue}>
                        {item.label}
                    </KislovDropdownItem>
                )}
            </div>
        </div>
    );
};
