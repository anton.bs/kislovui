import {ReactNode, useState, useRef} from 'react';
import './kislov-panel.scss';
import {KislovTitle} from "../KislovTitle/kislov-title";
import { FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCircleDown} from "@fortawesome/free-regular-svg-icons";
import {concatClasses} from "../../helpers";

interface KislovPanelProps {
    title: string,
    children: ReactNode,
    collapsible?: boolean,
    initialCollapseOpen?: boolean,
    customClasses?: string,
}
export const KislovPanel = ({
                           title, children, collapsible = false, initialCollapseOpen = true, customClasses
                       }: KislovPanelProps) => {

    const [collapse, setCollapse] = useState<boolean>(initialCollapseOpen ? !initialCollapseOpen : true);
    const panelBodyRef = useRef<HTMLDivElement | null>(null);

    return (
        <div className={concatClasses(["kislov-panel--wrapper", customClasses ? customClasses : ""])}>
            <div className="kislov-panel">
                <div className="kislov-panel--header">
                    <div className="kislov-panel--header-title">
                        <KislovTitle underline={!collapsible}>{title}</KislovTitle>
                    </div>
                    {collapsible &&
                        <div className={collapse ? "kislov-panel--header-collapse" : "kislov-panel--header-collapse open"} onClick={() => setCollapse(!collapse)}>
                            <FontAwesomeIcon icon={faCircleDown} />
                        </div>
                    }
                </div>
                <div ref={panelBodyRef} className={collapsible ? (collapse ? "kislov-panel--body" : "kislov-panel--body open") : "kislov-panel--body open"}>
                    {children}
                </div>
            </div>
        </div>
    );
};
