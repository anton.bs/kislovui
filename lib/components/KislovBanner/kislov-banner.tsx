import './kislov-banner.scss';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCircleXmark, faStar, faCircleDown} from "@fortawesome/free-regular-svg-icons";
import {faBullhorn, faTags, faChampagneGlasses, faMartiniGlassCitrus, faTruckFast, faGift} from "@fortawesome/free-solid-svg-icons";
import {ReactNode, useState} from "react";
import {concatClasses} from "../../helpers";

interface KislovBannerProps {
    text: string,
    children?: ReactNode,
    theme?: "red" | "green" | "yellow",
    icon?: "announcement" | "sale" | "wine" | "cocktail" | "delivery" | "star" | "gift",
    clickAction?: () => void,
    customClasses?: string,
}
export const KislovBanner = ({
                                text, children, theme = "red", icon, clickAction, customClasses = ""
                            }: KislovBannerProps) => {
    const [open, setOpen] = useState<boolean>(true);

    const getIcon = () => {
        switch (icon) {
            case "announcement":
                return <FontAwesomeIcon icon={faBullhorn} />
            case "sale":
                return <FontAwesomeIcon icon={faTags} />
            case "wine":
                return <FontAwesomeIcon icon={faChampagneGlasses} />
            case "cocktail":
                return <FontAwesomeIcon icon={faMartiniGlassCitrus} />
            case "delivery":
                return <FontAwesomeIcon icon={faTruckFast} />
            case "star":
                return <FontAwesomeIcon icon={faStar} />
            case "gift":
                return <FontAwesomeIcon icon={faGift} />
            default:
                return;
        }
    }

    return (
        <div className={concatClasses(["kislov-banner--wrapper", customClasses ? customClasses : ""])}>
          <div className={`kislov-banner kislov-banner--${theme}`} onClick={() => clickAction}>
              {children && <div className="close" onClick={() => setOpen(!open)}>{open ? <FontAwesomeIcon icon={faCircleXmark} /> : <FontAwesomeIcon icon={faCircleDown} />}</div> }
              {icon && <div className="kislov-banner--icon">{getIcon()}</div>}
              <div className="kislov-banner--content">
                  <div className="title">{text}</div>
                  {children && open && <div className="subtext">{children}</div> }
              </div>
          </div>
        </div>
    );
};
