import './kislov-tag.scss';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCircleXmark} from "@fortawesome/free-solid-svg-icons";
import {useState} from "react";
import {concatClasses} from "../../helpers";

interface KislovTagProps {
    tagText: string,
    variant?: "regular" | "outlined",
    size?: "small" | "medium" | "large",
    dismissible?: boolean,
    customClasses?: string,
}
export const KislovTag = ({
                                tagText, variant = "regular", size = "medium", dismissible = false, customClasses
                            }: KislovTagProps) => {
    const [dismissed, setDismissed] = useState<boolean>(false);

    if (!dismissed) {
    return (
        <div className={concatClasses([`kislov-tag ${variant} ${size}`, customClasses ? customClasses : ""])}>
            {tagText}
            {dismissible && <div className="close" onClick={() => setDismissed(!dismissed)}><FontAwesomeIcon icon={faCircleXmark} /></div> }
        </div>
    );
    } else {
        return null;
    }
};
