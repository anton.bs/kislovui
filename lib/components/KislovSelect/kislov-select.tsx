import {ReactNode, RefObject, useEffect, useRef, useState} from 'react';
import './kislov-select.scss';
import {KislovDropdown} from "../KislovDropdown/kislov-dropdown";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCircleChevronDown} from "@fortawesome/free-solid-svg-icons";
import {concatClasses} from "../../helpers";
import {SelectItem} from "../../types";

interface KislovSelectProps {
    label: string,
    items: SelectItem[],
    onChange?: (newValue: SelectItem) => void,
    selectedValue?: never,
    customClasses?: string,
    size?: "small" | "medium" | "large",
    disabled?: boolean
}

export const KislovSelect = ({
                                label, items = [], onChange, selectedValue, customClasses, size = "large", disabled = false
                            }: KislovSelectProps) => {

    const [dropdownOpen, setDropdownOpen] = useState<boolean>(false);
    const [currentItemLabel, setCurrentItemLabel] = useState<ReactNode | null>(null);
    const [currentItemValue, setCurrentItemValue] = useState<string | null>(selectedValue || null);
    const selectRef: RefObject<HTMLDivElement> = useRef(null);

    const changeValue = (selectedItem: SelectItem) => {
        setCurrentItemLabel(selectedItem.label);
        setCurrentItemValue(selectedItem.value);
        onChange?.(selectedItem)
    }

    const openDropdown = () => {
        if (!disabled) {
            setDropdownOpen(!dropdownOpen);
        }
    }

    useEffect(() => {
        const handleClick = (event: MouseEvent) => {
            if (selectRef.current && !selectRef.current.contains(event.target as Node)) {
                setDropdownOpen(false);
            }
        }
        document.addEventListener("mousedown", handleClick);

        return () => {
            document.removeEventListener("mousedown", handleClick)
        }
    }, [selectRef])

    return (
        <div className={concatClasses(["kislov-select--wrapper", customClasses ? customClasses : ""])}>
            <div ref={selectRef} className={`kislov-select ${size}${disabled ? ' disabled' : ''}`} onClick={() => openDropdown()}>
                <div className={`kislov-select--label${currentItemLabel ? ' touched' : ''}`}>{label}</div>
                {currentItemLabel && <div className={`kislov-select--current`}>{currentItemLabel}</div> }
                <div className="kislov-select--icon">
                    <FontAwesomeIcon icon={faCircleChevronDown} />
                </div>
                {dropdownOpen && <KislovDropdown size={size} items={items} selectionChange={(item) => changeValue(item)} selectedValue={currentItemValue} />}
            </div>
        </div>
    );
};
