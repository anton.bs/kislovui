import {ReactNode} from 'react';
import './kislov-feedback.scss';
import {concatClasses} from "../../helpers";

interface KislovFeedbackProps {
    children: ReactNode,
    variant?: "regular" | "success" | "warning" | "error",
    title: string,
    dismissible?: boolean,
    customClasses?: string,
}
export const KislovFeedback = ({
                                children, variant = "regular", title, dismissible = false, customClasses
                            }: KislovFeedbackProps) => {

    return (
        <div className={concatClasses(["kislov-feedback--wrapper", customClasses ? customClasses : ""])}>
            <div className={`kislov-feedback--box ${variant}`}>
                {dismissible && <div className="kislov-feedback--close">&#x2715;</div>}
                <div className="kislov-feedback--title">
                    {title}
                </div>
                <div className="kislov-feedback--content">
                    {children}
                </div>
            </div>
        </div>
    );
};
