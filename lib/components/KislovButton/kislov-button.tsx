import {ReactNode} from 'react';
import './kislov-button.scss';
import {concatClasses} from "../../helpers";

interface KislovButtonProps {
    children: ReactNode,
    variant?: "regular" | "outlined" | "text",
    isIcon?: boolean,
    size?: "small" | "medium" | "large",
    iconLeft?: ReactNode,
    iconRight?: ReactNode,
    disabled?: boolean,
    clickAction?: () => void,
    customClasses?: string,
}
export const KislovButton = ({
                                children, variant = "regular", size = "large", isIcon = false, iconLeft, iconRight, disabled = false, clickAction, customClasses
                            }: KislovButtonProps) => {

    return (
        <div className={concatClasses(["kislov-button--wrapper", customClasses ? customClasses : ""])}>
            <button disabled={disabled} className={`kislov-button--${variant} ${size}${isIcon ? " icon" : ""}`} onClick={clickAction}>
                {iconLeft}
                {children}
                {iconRight}
            </button>
        </div>
    );
};
