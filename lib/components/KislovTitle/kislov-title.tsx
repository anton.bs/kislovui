import {ReactNode} from 'react';
import './kislov-title.scss';
import {concatClasses} from "../../helpers";

interface KislovTitleProps {
    children: ReactNode,
    underline?: boolean,
    customClasses?: string,
}
export const KislovTitle = ({
                                children,
    underline = true,
    customClasses
                            }: KislovTitleProps) => {

    return (
        <div className={concatClasses(["kislov-title--wrapper", customClasses ? customClasses : ""])}>
            <div className={concatClasses(["kislov-title", underline ? "underline" : ""])}>
                {children}
            </div>
        </div>
    );
};
