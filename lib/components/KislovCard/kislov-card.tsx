import React, {ReactNode, RefObject, useEffect, useRef, useState} from 'react';
import './kislov-card.scss';
import {KislovButton} from "../KislovButton/kislov-button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faHeart} from "@fortawesome/free-solid-svg-icons";
import {KislovTag} from "../KislovTag/kislov-tag";
import {concatClasses} from "../../helpers";

interface KislovCardProps {
    children: ReactNode,
    image?: string,
    imageOpacity?: boolean,
    title: string,
    tag?: string,
    actionButton?: string,
    actionButtonOnClick?: () => void,
    likable?: boolean,
    clickable?: boolean,
    clickAction?: () => void,
    colSize?: number,
    fixedHeight?: boolean,
    customClasses?: string,
}
export const KislovCard = ({
                                children,
                               image,
                               imageOpacity = false,
                               title,
                               tag,
                               actionButton,
                               actionButtonOnClick,
                               likable = false,
                               clickable = false,
                               clickAction,
                               customClasses,
    colSize = 3,
    fixedHeight = true,
                            }: KislovCardProps) => {

    const getClampLines = () => {
        let linesToClamp = 0;
        if (image) {
            linesToClamp += 7;
        }
        if (tag) {
            linesToClamp += 2;
        }
        if (actionButton || likable) {
            linesToClamp += 1;
        }

        return linesToClamp;
    }
    const styling = {lineClamp: (!fixedHeight ? 0 : 16 - getClampLines())}

    const titleRef: RefObject<HTMLDivElement> = useRef(null);
    const [showTooltip, setShowTooltip] = useState<boolean>(false);

    useEffect(() => {
        const checkOverflow = () => {
            if (titleRef.current) {
                const titleEl = titleRef.current;
                const isOverflown = titleEl.scrollWidth > titleEl.clientWidth;
                setShowTooltip(isOverflown);
            }
        }
        checkOverflow();

        window.addEventListener('resize', checkOverflow);

        return () => {
            window.removeEventListener('resize', checkOverflow);
        }
    }, [titleRef]);

    return (
        <div className={concatClasses([`kislov-card--wrapper col-${colSize}`, customClasses ? customClasses : ""])}>
            <div className={concatClasses(["kislov-card", fixedHeight ? "fixed-height" : "", clickable ? "clickable" : ""])} onClick={clickable ? clickAction : () => {return}}>
                {image && <div className={`kislov-card--image${imageOpacity ? " opacity" : ""}`}>
                    <img src={image} alt={title} />
                </div> }
                <div className={`kislov-card--text${!image ? ' extra-padding' : ''}`}>
                    <div ref={titleRef} className={`kislov-card--text--title`}>
                        {title}
                        {showTooltip &&
                            <div className={"kislov-card--text--title--tooltip"}>
                                {title}
                            </div>
                        }
                    </div>
                    {tag && <KislovTag tagText={tag} size="small" />}
                    <div className="kislov-card--text--content" style={{"--clampLines": styling.lineClamp} as React.CSSProperties}>{children}</div>
                </div>
                <div className="kislov-card--actions">
                    {actionButton && <KislovButton variant="outlined" size="small" clickAction={actionButtonOnClick ? actionButtonOnClick : () => {return}}>{actionButton}</KislovButton>}
                    {likable && <KislovButton size="small" isIcon><FontAwesomeIcon icon={faHeart} /></KislovButton>}
                </div>
            </div>
        </div>
    );
};
