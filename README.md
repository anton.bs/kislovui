# Kislov Winery UI Library

This is a UI library for ReactJS project created for a private project for Kislov Family Winery.

## Installation

Run ```npm i kislovui```

### Dependencies
- Font Awesome - ```@fortawesome/fontawesome-svg-core```
- Font Awesome - ```@fortawesome/free-brands-svg-icons```
- Font Awesome - ```@fortawesome/free-regular-svg-icons```
- Font Awesome - ```@fortawesome/free-solid-svg-icons```
- Font Awesome - ```@fortawesome/react-fontawesome```
- SASS Module - ```sass```

## Currently Included Components

### Kislov Banner
A component for showing important information and emphasize it.
Takes 100% of available width.
Has 3 themes: red (default), green and yellow (which is actually pink).
Can have an icon from a list of predefined icons.

### KislovButton
Has 3 variants: regular, outlined and text.
Has 3 sizes: small, medium and large.

### KislovCard
Card may contain header graphic, a tag, a footer button and a like button.
Is of same height - body text will be line-clamped according to available space.

### KislovFeedback
A component for showing feedback messages.
Supports 4 variants: regular - blue shades, success - green shades, warning - yellow shades, error - red shades.

### KislovInput
A component to get user input in a single line input component or with multiline prop - to get multiline textarea.
Default lines for multilne is 3, however user can adjust by passing ```rows``` prop.

### KislovPanel
A component for ordering data, like a section panel.
It has a title and a body.
Can be collapsible - then there is a open or collapse button on the right top corner.

### KislovSelect
A component for a select menu.

### KislovTag
Simple highlighted tag. Can be dismissible - then it disappears on clicking dismiss button.