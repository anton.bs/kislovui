import {KislovPanel} from "../lib/components/KislovPanel/kislov-panel";
import {KislovButton} from "../lib/components/KislovButton/kislov-button";
import {KislovCard} from "../lib/components/KislovCard/kislov-card";
import SampleIMG from "./assets/sampleImage.JPG";
import {KislovFeedback} from "../lib/components/KislovFeedback/kislov-feedback";
import {KislovTag} from "../lib/components/KislovTag/kislov-tag";
import {KislovBanner} from "../lib/components/KislovBanner/kislov-banner";
import {KislovInput} from "../lib/components/KislovInput/kislov-input";
import {KislovSelect} from "../lib/components/KislovSelect/kislov-select";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faWineGlass, faRankingStar, faCheck, faEnvelope} from "@fortawesome/free-solid-svg-icons";
import {ReactNode} from "react";

function App() {

    const dropdownItems: {label: ReactNode, value: string}[] = [
        {
            label: <><FontAwesomeIcon icon={faWineGlass} /> Our Wines</>,
            value: "our-wines"
        },
        {
            label: <><FontAwesomeIcon icon={faRankingStar} /> Best Sellers</>,
            value: "best-sellers"
        },
        {
            label: <><FontAwesomeIcon icon={faCheck} /> Terms of Service</>,
            value: "terms-of-service"
        },
        {
            label: <><FontAwesomeIcon icon={faEnvelope} /> Contact</>,
            value: "contacts"
        },
    ]

  return (
    <>
      <div style={{width: "100%", padding: "0 50px"}}>
          <strong>Kislov Panel</strong>
          <KislovPanel title="Hi! I am Kislov Panel">
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur maximus enim in tortor tincidunt tristique sit amet nec mauris. Duis id ex ipsum. Suspendisse ut convallis massa, at sollicitudin nisl. Nunc lacinia dui eu laoreet elementum. Vestibulum dignissim viverra mauris, faucibus laoreet magna porttitor vitae. Proin consequat vehicula molestie. Nam eget suscipit lorem, vel laoreet velit. Phasellus auctor magna ut suscipit dignissim.
          </KislovPanel>
          <br/><br/>
          <strong>Kislov Panel - Collapse</strong>
          <KislovPanel collapsible title="Hi!!! I am collapsible panel" initialCollapseOpen={false}>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur maximus enim in tortor tincidunt tristique sit amet nec mauris. Duis id ex ipsum. Suspendisse ut convallis massa, at sollicitudin nisl. Nunc lacinia dui eu laoreet elementum. Vestibulum dignissim viverra mauris, faucibus laoreet magna porttitor vitae. Proin consequat vehicula molestie. Nam eget suscipit lorem, vel laoreet velit. Phasellus auctor magna ut suscipit dignissim.
          </KislovPanel>

          <br/><br/>
          <strong>Kislov Button Regular</strong>
          <br/>
          <KislovButton>I AM A BUTTON</KislovButton>
          <KislovButton size="medium">I AM A BUTTON</KislovButton>
          <KislovButton size="small">I AM A BUTTON</KislovButton>
          <KislovButton size="small" disabled>I AM A BUTTON</KislovButton>
          <br/>
          <br/>
          <strong>Kislov Button Outlined</strong>
          <br/>
          <KislovButton variant="outlined">I AM A BUTTON</KislovButton>
          <KislovButton variant="outlined" size="medium">I AM A BUTTON</KislovButton>
          <KislovButton variant="outlined" size="small">I AM A BUTTON</KislovButton>
          <KislovButton variant="outlined" size="small" disabled>I AM A BUTTON</KislovButton>
          <br/>
          <br/>
          <strong>Kislov Button Text</strong>
          <br/>
          <KislovButton variant="text">I AM A BUTTON</KislovButton>
          <KislovButton variant="text" size="medium">I AM A BUTTON</KislovButton>
          <KislovButton variant="text" size="small">I AM A BUTTON</KislovButton>
          <KislovButton variant="text" size="small" disabled>I AM A BUTTON</KislovButton>


          <br/>
          <br/>
          <div style={{width: "100%", display: "flex", flexDirection: "row", flexWrap: "wrap"}}>
              <KislovCard colSize={4} clickable clickAction={() => console.log("Clicked card!")} image={SampleIMG} title="Card Title">
                  Suspendisse bibendum tellus placerat ipsum sodales venenatis. Vivamus hendrerit vehicula mauris, sit amet porta augue. Aliquam bibendum, lacus ut bibendum condimentum, sem magna volutpat purus, vestibulum facilisis tortor metus at lectus.
              </KislovCard>
              <KislovCard fixedHeight={false} colSize={4} title="No graphics card!" actionButton="Click me!" actionButtonOnClick={() => console.log("Clicked the button")}>
                  Suspendisse bibendum tellus placerat ipsum sodales venenatis. Vivamus hendrerit vehicula mauris, sit amet porta augue. Aliquam bibendum, lacus ut bibendum condimentum, sem magna volutpat purus, vestibulum facilisis tortor metus at lectus.
                  Suspendisse bibendum tellus placerat ipsum sodales venenatis. Vivamus hendrerit vehicula mauris, sit amet porta augue. Aliquam bibendum, lacus ut bibendum condimentum, sem magna volutpat purus, vestibulum facilisis tortor metus at lectus.
                  Suspendisse bibendum tellus placerat ipsum sodales venenatis. Vivamus hendrerit vehicula mauris, sit amet porta augue. Aliquam bibendum, lacus ut bibendum condimentum, sem magna volutpat purus, vestibulum facilisis tortor metus at lectus.
                  Suspendisse bibendum tellus placerat ipsum sodales venenatis. Vivamus hendrerit vehicula mauris, sit amet porta augue. Aliquam bibendum, lacus ut bibendum condimentum, sem magna volutpat purus, vestibulum facilisis tortor metus at lectus.
                  Suspendisse bibendum tellus placerat ipsum sodales venenatis. Vivamus hendrerit vehicula mauris, sit amet porta augue. Aliquam bibendum, lacus ut bibendum condimentum, sem magna volutpat purus, vestibulum facilisis tortor metus at lectus.
                  Suspendisse bibendum tellus placerat ipsum sodales venenatis. Vivamus hendrerit vehicula mauris, sit amet porta augue. Aliquam bibendum, lacus ut bibendum condimentum, sem magna volutpat purus, vestibulum facilisis tortor metus at lectus.
              </KislovCard>
              <KislovCard colSize={4} tag="Best Seller!" image={SampleIMG} imageOpacity title="Hello Kislov Card! This is a very long title" actionButton="Read More" actionButtonOnClick={() => console.log("Clicked read more")}>
                  Suspendisse bibendum tellus placerat ipsum sodales venenatis. Vivamus hendrerit vehicula mauris, sit amet porta augue. Aliquam bibendum, lacus ut bibendum condimentum, sem magna volutpat purus, vestibulum facilisis tortor metus at lectus.
              </KislovCard>
              <KislovCard colSize={4} title="No graphic and no buttons this is a much more longer title than the previous one, in order to check the tooltip">
                  Suspendisse bibendum tellus placerat ipsum sodales venenatis. Vivamus hendrerit vehicula mauris, sit amet porta augue. Aliquam bibendum, lacus ut bibendum condimentum, sem magna volutpat purus, vestibulum facilisis tortor metus at lectus.
                  Suspendisse bibendum tellus placerat ipsum sodales venenatis. Vivamus hendrerit vehicula mauris, sit amet porta augue. Aliquam bibendum, lacus ut bibendum condimentum, sem magna volutpat purus, vestibulum facilisis tortor metus at lectus.
              </KislovCard>
          </div>
      </div>
        <div style={{width: "100%", padding: "20px", display: "flex"}}>
            <KislovFeedback title="Refular Feedback Message">
                Text of the feedback. Suspendisse bibendum tellus placerat ipsum sodales venenatis. Vivamus hendrerit vehicula mauris, sit amet porta augue. Aliquam bibendum, lacus ut bibendum condimentum.
            </KislovFeedback>
            <KislovFeedback title="Success Feedback Message" variant="success">
                Text of the feedback. Suspendisse bibendum tellus placerat ipsum sodales venenatis. Vivamus hendrerit vehicula mauris, sit amet porta augue. Aliquam bibendum, lacus ut bibendum condimentum.
            </KislovFeedback>
            <KislovFeedback title="Warning Feedback Message" variant="warning">
                Text of the feedback. Suspendisse bibendum tellus placerat ipsum sodales venenatis. Vivamus hendrerit vehicula mauris, sit amet porta augue. Aliquam bibendum, lacus ut bibendum condimentum.
            </KislovFeedback>
            <KislovFeedback title="Error Feedback Message" variant="error">
                Text of the feedback. Suspendisse bibendum tellus placerat ipsum sodales venenatis. Vivamus hendrerit vehicula mauris, sit amet porta augue. Aliquam bibendum, lacus ut bibendum condimentum.
            </KislovFeedback>
        </div>
        <div style={{position: "relative", width: "300px"}}>
            <KislovSelect label="Choose Your Option" items={[]} />
            <KislovSelect label="Choose Your Option" items={dropdownItems} />
        </div>
        <div>
            <KislovTag tagText="Regular tag" size="small" />
            <KislovTag tagText="Regular tag" size="small" dismissible />
            <KislovTag tagText="Regular tag" size="medium" />
            <KislovTag tagText="Regular tag" size="large" />
            <KislovTag tagText="Regular tag" size="large" dismissible />
        </div>
      <div>
            <KislovTag tagText="Outlined tag" variant="outlined" size="small" />
            <KislovTag tagText="Outlined tag" variant="outlined" size="medium" />
            <KislovTag tagText="Outlined tag" variant="outlined" size="large" />
            <KislovTag tagText="Outlined tag" variant="outlined" size="large" dismissible />
        </div>
        <div>
            <KislovBanner text="New Website for Kislov Winery!!!!" icon="star" />
            <KislovBanner text="New Website for Kislov Winery!!!!" theme="green" icon="gift" />
            <KislovBanner text="New Website for Kislov Winery!!!!" theme="yellow" icon="announcement" />
            <KislovBanner text="New Website for Kislov Winery!!!!" theme="green" icon="sale" customClasses="custom-class-check">
                Only by the end of March - special sale on all Kislov Wines - <strong>UP TO 50% OFF</strong>!
            </KislovBanner>
        </div>
        <div style={{display: "flex", width: "50%", flexWrap: "wrap"}}>
            <KislovInput label="Default Large" value="" />
            <KislovInput label="Medium" value="My name is Anton" size="medium" />
            <KislovInput label="Small" value="" size="small" />
            <KislovSelect label={"Select Default Large"} items={[{label: "Item 1", value: "1"}, {label: "Item 2", value: "2"}]} />
            <KislovSelect size={"medium"} label={"Select Medium"} items={[{label: "Item 1", value: "1"}, {label: "Item 2", value: "2"}]} />
            <KislovSelect size={"small"} label={"Select Small"} items={[{label: "Item 1", value: "1"}, {label: "Item 2", value: "2"}]} />
            <KislovSelect size={"small"} label={"Select Small Disabled"} disabled items={[{label: "Item 1", value: "1"}, {label: "Item 2", value: "2"}]} />
            <KislovInput label="Default Large" value="Predefined value" disabled />
            <KislovInput label="Default Large Disabled no Value" disabled />
            <KislovInput label="Multiline box" type="multiline" />
            <KislovInput label="Default Large" type="multiline" value="Predefined value" disabled />
            <KislovInput label="Medium" type="multiline" size="medium" value="Predefined value" />
            <KislovInput label="Small Disabled" type="multiline" size="small" value="Predefined value" disabled />
            <KislovInput label="Password" type="password" />
        </div>
    </>
  )
}

export default App
